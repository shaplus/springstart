package com.mycomp.springstart.entities;




import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ProductEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String product_name;
    private String product_group;
    private String store;

    private int count;

    public ProductEntity(){

    }

    public ProductEntity(String product_name, String  product_group, String  store, int count) {
        this.product_name = product_name;
        this.product_group = product_group;
        this.store = store;
        this.count = count;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_group() {
        return product_group;
    }

    public String getStore() {
        return store;
    }

    public int getCount() {
        return count;
    }



    public void setCount(int count) {
        this.count = count;
    }
}
