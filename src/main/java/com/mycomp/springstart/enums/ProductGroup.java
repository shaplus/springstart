package com.mycomp.springstart.enums;



public enum ProductGroup {
    STATIONERY ("Stationery"),
    REPAIR ("Repair"),
    FLOOR_COVERING ("Floor coverage"),
    PLUMBING ("Plumbing"),
    FURNITURE ("Furniture"),
    GARDEN_PRODUCTS ("Garden products"),
    PET_PRODUCTS ("Pet products"),
    LIGHTING ("Lighting"),
    HOUSE_PRODUCTS ("House products"),
    TOOLS ("Tools"),
    HOUSEHOLD_APPLIANCES ("Household appliances"),
    ELECTRONICS ("Electronics"),
    CHILDRENS_GOODS ("Children goods"),
    BEAUTY_AND_HEALTH ("Beauty and health"),
    FOOD_AND_DRINKS ("Food and drinks"),
    CLOTHING ("Clothing"),
    SPORTS_GOODS ("Sports goods"),
    TOURISM_AND_MILITARY ("Tourism and the military"),
    HOUSEHOLD_CHEMICALS ("Household chemicals");

    ProductGroup(String stationery) {
    }

    public static int getSize(){
        return ProductGroup.values().length;
    }


}
