package com.mycomp.springstart.enums;

public enum Stores {
    Cherkay,
    Chernihiv,
    Chernivtshi,
    Dnipro,
    Donetsk,
    Ivano_Frankivsk,
    Kharkiv,
    Kherson,
    Khmelnytskyi,
    Kyiv,
    Kropyvnytskyi,
    Luhansk,
    Lviv,
    Mikholaiv,
    Odesa,
    Poltava,
    Rivne,
    Sumy,
    Ternopil,
    Vinnitsia,
    Lutsk,
    Uzhorod,
    Zaporizhzhia,
    Zhytomyr,
    Sevastopol;

    public static int getSize(){
        return Stores.values().length;
    }
}
