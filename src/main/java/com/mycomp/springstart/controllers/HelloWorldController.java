package com.mycomp.springstart.controllers;

import com.mycomp.springstart.entities.ProductEntity;
import com.mycomp.springstart.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;

@RestController
public class HelloWorldController {

    @Autowired
    ProductService productService;

    @GetMapping("/hello")
    public String helloWorld(){
        return "Hello World";
    }

    @GetMapping("/products")
    public List<ProductEntity> getProducts(){
        return productService.getProducts();
    }

    @GetMapping("/products/{id}")
    public ProductEntity getOneProducts(@PathVariable Integer id){
        return productService.getOneProduct(id);
    }

    @PostMapping("/products")
    public void addProduct(@RequestBody ProductEntity product){
        productService.addProduct(product);
    }

    @PutMapping("/products/{id}")
    public void updateProduct (@PathVariable Integer id, @RequestBody ProductEntity productEntity){
        productService.updateProduct(productEntity);
    }

    @DeleteMapping("/products/{id}")
    public List<ProductEntity> deleteProduct(@PathVariable Integer id){
        productService.deleteProduct(id);
        return productService.getProducts();
    }


}
