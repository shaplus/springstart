package com.mycomp.springstart.services;

import com.mycomp.springstart.entities.ProductEntity;
import com.mycomp.springstart.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;
//    private static List<ProductEntity> list = new ArrayList<>(Arrays.asList(
//            new ProductEntity("namee", "FLOOR_COVERING", "Kharkiv", 5),
//            new ProductEntity("namee2", "FURNITURE", "Odesa", 10)
//    ));

    public List<ProductEntity> getProducts() {
        //return list;
        return productRepository.findAll();
    }

    public ProductEntity getOneProduct(Integer id) {
        //return list.stream().filter(x -> Objects.equals(x.getProduct_name(), product)).findFirst().orElse(null);
        return productRepository.findById(id).orElse(null);
    }

    public void addProduct(ProductEntity product) {
        //list.add(product);
        productRepository.save(product);
    }

    public void updateProduct(ProductEntity product) {
//        List<ProductDTO> tempList = new ArrayList<>();
//        for(ProductDTO prd : list){
//            if(prd.getProduct_name().equals(productName)){
//                tempList.add(product);
//            }else {
//                tempList.add(prd);
//            }
//        }
//        list = tempList;
        productRepository.save(product);
    };




    public void deleteProduct(Integer id){
//        List<ProductEntity> tempList = new ArrayList<>();
//        for(ProductEntity prd : list){
//            if(!prd.getProduct_name().equals(productName)){
//                tempList.add(prd);
//            }
//        }
//        list = tempList;
        productRepository.deleteById(id);
    }

    public List<ProductEntity> findAllB

    ;

}
